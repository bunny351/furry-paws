%
% main.fp - main module
%

% notes:
%
% - keyboard controls suck, try mouse or keep speed
% - define proper state with accessors later
% - practically everything is missing


w = ~600
h = ~500

"../../lib/sqrt.fp"
"../../lib/graphics.fp"
"scaled.fp"
"vector.fp"
"screen.fp"
"engine.fp"

% state = <direction, speed, <player, object1, ...>>

main = g:graphics.screen:size;
       g:keyrepeat.~<1, 1>;
       loop.[~0, ~0, al.[player_object, @test_object.iota.~10]];
       exit.(return ~0)

% loop : state -> state'
loop = scroll_objects +
       (redraw_objects;
        [g:event.~F, current_direction, current_speed, e:tick.current_objects] +
        (atom.x -> loop.tl;
          [|?QUIT|].x -> tl;
  	  [|?KEY, not, ?27, #|].x -> tl;
  	  [|?KEY, id, ?273, #|].x -> loop.player_accdec.[~2, tl]; % UP
 	  [|?KEY, id, ?274, #|].x -> loop.player_accdec.[~-2, tl]; % DOWN
 	  [|?KEY, id, ?275, #|].x -> loop.player_turn.[~1, tl]; % RIGHT
 	  [|?KEY, id, ?276, #|].x -> loop.player_turn.[~-1, tl]; % LEFT
          loop.tl) )

redraw_objects = g:render.@e:object_graphics.clip.current_objects

scroll_objects = [current_direction, current_speed,
	          al.[s1.current_objects,
		      e:scroll.[v:mul_sv.[current_speed, v:direction_v.current_direction],
		                tl.current_objects]]]

current_direction = s1
current_speed = s2
current_objects = s3

player_object = e:make_object.[~PLAYER, screen:center, ~<0, 0>,
      		               ~"icons/cannon-1.png",
		               [], ~0] 
test_object = e:make_object.[~TEST, random_position,
	          	     ~<0, 0>, %random_direction,
			     ~"icons/shot1.png", [], ~0]

player_icons = ~<`icons/cannon-1.png`,
	         `icons/cannon-2.png`,
	         `icons/cannon-3.png`,
	         `icons/cannon-4.png`,
	         `icons/cannon-5.png`,
	         `icons/cannon-6.png`,
	         `icons/cannon-7.png`,
	         `icons/cannon-8.png`,
	         `icons/cannon-9.png`,
	         `icons/cannon-10.png`,
	         `icons/cannon-11.png`,
	         `icons/cannon-12.png`,
	         `icons/cannon-13.png`,
	         `icons/cannon-14.png`,
	         `icons/cannon-15.png`,
	         `icons/cannon-16.png`>

player_update_icon =
   [current_direction,
    current_speed,
    al.[e:object_update_shape.[
         select.[min.[~15, add1.div.[current_direction, ~22]], % too rough
	         player_icons],
	 s1.current_objects],
       tl.current_objects] ]

player_accdec = [current_direction.y, 
	         trace SPEED.max.[minspeed, s:add_s.[x, current_speed.y]], 
		 current_objects.y]

minspeed = ~-10

player_turn = player_update_icon.
	        [trace DIR.s:normalize_deg.s:add_s.[current_direction.y, x],
	         current_speed.y,
	         current_objects.y]

clip = compress.[@e:clip.dl.[screen:screen_rect, id], id]

random_direction = v:direction_v._rnd.~360
random_position = [s:scale._rnd.w, s:scale._rnd.h]
