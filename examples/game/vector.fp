%
% vector.fp - (scaled) vector operations
%

% requires: scaled (s)

% vector = <x-scaled, y-scaled>
% rect = <topleft, bottomright>


v = {

  rect_topleft = s1
  rect_bottomright = s2

  % rect : <x1, y1, x2, y2> -> rect
  % rect : <topleft, bottomright> -> rect
  rect = eq.[len, ~2] -> id; rect.pair

  % add_v : <vector1, vector2> -> vector
  % sub_v : <vector1, vector2> -> vector
  add_v = [s:add_s.[x.x, x.y], 
  	   s:add_s.[y.x, y.y] ]
  sub_v = [s:sub_s.[x.x, x.y], 
  	   s:sub_s.[y.x, y.y] ]

  % mul_sv : <scaled, vector> -> vector'
  mul_sv = [s:mul_s.[x, x.y], s:mul_s.[x, y.y]]

  % unit_v : _ -> vector
  unit_v = [~0, neg.s:one]	% points up

  % length : vector -> scaled
  % distance : <vector1, vector2> -> vector
  length = sqrt.sub.@square
  distance = length.sub_v

  % inside : <vector, rect> -> bool
  inside = ge.[x.x, x.rect_topleft.y] &
           ge.[y.x, y.rect_topleft.y] &
           lt.[x.x, x.rect_bottomright.y] &
           lt.[y.x, y.rect_bottomright.y]

  % direction_v : deg -> vector
  direction_v = [s:sin, s:sin.add.[id, ~270]]

  % turn_v : vector -> vector'
  turn_v = @neg

  % rotate_v : <deg, vector> -> vector'
  rotate_v = [s:sub_s.[s:mul_s.[x.y, s:cos.x],
  	               s:mul_s.[y.y, s:sin.x] ],
              s:add_s.[s:mul_s.[x.y, s:sin.x],
  	               s:mul_s.[y.y, s:cos.x] ] ]

  % helper functions
  square = s:mul_s.[id, id]

}
