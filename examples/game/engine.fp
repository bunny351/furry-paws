%
% engine.fp - sprite engine
%

% object = <id, age, position, vector, shape, props, range>
% position = vector = <x-scaled, y-scaled>
% props = <prop1, ...>
% prop = <name, value>


e = {

  object_id = s1
  object_age = s2
  object_position = s3
  object_vector = select.[~4, id]
  object_shape = select.[~5, id]
  object_props = select.[~6, id]
  object_range = select.[~7, id]

  object_update_shape = put.al.[~5, id]

  % make_object : <id, position, vector, shape, props, range>
  make_object = cat.[[x, ~0], tl]

  % move : object -> object'
  move = move_by.[object_vector, id]

  % move_by : <vector, object> -> object'
  move_by = put.[~3, v:add_v.[object_position.y, x], y]

  % place : <position, object> -> object'
  place = put.al.[~3, id]

  % object:graphics : object -> graphics
  object_graphics = [~IMAGE, object_shape, 
  	             s:unscale.x.object_position,
  	             s:unscale.y.object_position]

  % tick : objects -> objects'
  tick = @(put.[~2, add1.object_age, id].move)

  % clip : <rect, object> -> object | F
  clip = v:inside.[object_position.y, x] & id

  % scroll : <vector, objects> -> objects'
  % scroll_relative : <object(center), objects> -> objects'
  scroll = @(place.[v:sub_v.[object_position.y, x], y]).dl
  scroll_relative = scroll.[v:turn_v.object_position.x, y]

  % collisions : objects -> <... sequence of colliding pairs ...>
  collisions = cat.@colliding1.spread
  colliding1 = [x, colliding1'] + (null.y -> []; dl)
  colliding1' = null.y -> [];
  	        eq.[x, s1.y] -> colliding1'.[x, tl.y];
	        le.[v:distance.[object_position.x, object_position.y],
	            min.[object_range.x, object_range.y] ] ->
		  al.[y, colliding1.[x, tl.y]];
		colliding1.[x, tl.y]

  % transitions : <objects, transition-table> -> objects'
  % transition-table = <<id, min-age, new-id>, ...>
  % id = newid = atom | F
  transitions = compress.[id, id].@transition1.dr
  % transition1 : <object, t-table> -> object'
  transition1 = [x, y, ~1] +
  	        while le.[z, len.y] 
		  ((not.s1.select.[z, y] | eq.[object_id.x, s1.select.[z, y]]) & 
		    ge.[object_age.x, s2.select.[z, y]] ->
		      (s3.select.[z, y] & [put.[~1, s3.select.[z, y], x], y, ~1]);
		      [x, y, add.z] )

}
