%
% screen.fp - screen parameters
%

% requires: vector (v)

screen = {

  w = ~600
  h = ~500

  size = [w, h]
  screen_rect = v:rect.[~<0, 0>, @s:scale.size]
  on_screen = v:inside.[id, screen_rect]

  random_position = [s:scale.rnd.w, s:scale.rnd.h]

  center = [s:scale.div.[w, ~2], s:scale.div.[h, ~2]]

}
