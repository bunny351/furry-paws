%
% dirwalk.fp - walk directories like find(1)
%
% usage: dirwalk [DIR]
%


"lib/shellutils.fp"


main = walkd.[([|#|].x -> tos.x; ~"."), y]

% walkd : <path, io> -> <_, io'>
walkd = [x, sh:stat] +
        [|?DIR, ..|].s1.y -> 
	  walkdr.[x, sh:directory.[x, s2.y]];
	  emit.[cat.[x, ~<10>], s2.y]

% walkdr : <path, <directory, io>> -> <_, io'>
walkdr = [x, @cat.dl.[cat.[x, ~<$/>], s1.y], s2.emit.[cat.[x, ~<$/, 10>], s2.y]] +
         [#, \(y.walkd.flip).al.[z, y]]
