%
% configfile.fp
%
% Code for parsing configuration files
%

% requires: strings.fp

%
%  Configuration format:
%
%    config = <<group1, <var1/1, val1/1>, ...>, ...>
%


configfile = {

  % load : <path, io> -> <config, io'>
  load = in +
         [group.parse.filter.@str:trim.str:lines.x, y]

  filter = compress.[@(not.null & not.eq.[~$#, s1]), id]

  parse = @parse'
  parse' = [id, index.[~$=, id]] +
   	   y -> [toa.str:trimright.take.[sub1.y, x],
	         str:trimleft.drop.[y, x] ];
	   eq.[~$[, s1.x] & eq.[~$], s1r.x] ->
	     toa.str:trim.tl.tlr.x;
	   throw.[~INVALID, x]

  group = trans.[al.[~DEFAULT, compress.[@atom, id]],
  	  	 split.[~F, @(atom -> ~F)] ]

}
