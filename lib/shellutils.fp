% shellutils.fp
%
% Provides:
%
%   sh:directory : <dir, io> -> <<file1, ...>, io'>
%   sh:run : <command, io> -> <command, io'>
%   sh:cd : <string, io> -> <string, io'>
%   sh:mkdir : <string, io> -> <string, io'>
%   sh:rmdir : <string, io> -> <string, io'>
%   sh:cwdir : <_, io> -> <string, io'>
%   sh:rm : <string, io> -> <string, io'>
%   sh:stat : <string, io> -> <<type, size, mtime>, io'>
%   sh:lstat : <string, io> -> <<type, size, mtime>, io'>
%   sh:time : <_, io> -> <mtime, io'>
%   sh:sleep : <secs, io> -> <secs, io'>
%
%   mtime = <hi, lo>


"lib/shellutils.c"


sh = {

  directory = [extern `f_directory`.x, _iostep.[y, ~`directory`]]
  run = [x, system] +
        zero.s1.y -> [x, s2.y];
	  fail.cat.[~"shell command terminated with non-zero exit code ",
	             tos.s1.y, ~": ", tos.x]
  cd = [extern `f_cd`.x, _iostep.[y, ~`cd`]]
  mkdir = [extern `f_mkdir`.x, _iostep.[y, ~`mkdir`]]
  rmdir = [extern `f_rmdir`.x, _iostep.[y, ~`rmdir`]]
  cwd = [extern `f_cwd`.x, _iostep.[y, ~`cwd`]]
  rm = [extern `f_rm`.x, _iostep.[y, ~`rm`]]
  stat = [extern `f_stat`.x, _iostep.[y, ~`stat`]]
  lstat = [extern `f_lstat`.x, _iostep.[y, ~`lstat`]]
  time = [extern `f_time`.x, _iostep.[y, ~`time`]]
  sleep = [extern `f_sleep`.x, _iostep.[y, ~`sleep`]]

}
