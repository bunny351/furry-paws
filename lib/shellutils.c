/* shellutils.c */


#include <fp/fp.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#ifdef __MINGW32__
# include <direct.h>
# include <io.h>

# define lstat      stat

struct dirent
{
    char *d_name;
};

typedef struct
{
    struct _finddata_t	fdata;
    int			handle;
    struct dirent	current;
} DIR;


static DIR *
opendir(const char *name)
{
    int name_len = strlen(name);
    DIR *dir = (DIR *)malloc(sizeof(DIR));
    char *what;
    if (!dir)
    {
	errno = ENOMEM;
	return NULL;
    }
    what = (char *)malloc(name_len + 3);
    if (!what)
    {
	free(dir);
	errno = ENOMEM;
	return NULL;
    }
    strcpy(what, name);
    if (strchr("\\/", name[name_len - 1]))
	strcat(what, "*");
    else
	strcat(what, "\\*");

    dir->handle = _findfirst(what, &dir->fdata);
    if (dir->handle == -1)
    {
	free(what);
	free(dir);
	return NULL;
    }
    dir->current.d_name = NULL; /* as the first-time indicator */
    free(what);
    return dir;
}

static int
closedir(DIR * dir)
{
    if (dir)
    {
	int res = _findclose(dir->handle);
	free(dir);
	return res;
    }
    return -1;
}

static struct dirent *
readdir(DIR * dir)
{
    if (dir)
    {
	if (!dir->current.d_name /* first time after opendir */
	     || _findnext(dir->handle, &dir->fdata) != -1)
	{
	    dir->current.d_name = dir->fdata.name;
	    return &dir->current;
	}
    }
    return NULL;
}

#else
# include <dirent.h>
#endif


XDEFINE(f_directory)
{
  DIR *dir;
  struct dirent *de;
  int i = 0, len = 10;
  char **dirbuf = (char **)malloc(sizeof(char *) * len);

  if(dirbuf == NULL)
    bomb("out of memory - can not allocate directory buffer");

  dir = opendir(check_string(x, "directory", 0));

  if(dir == NULL) 
    failx(x, strerror(errno), "directory");

  for(;;) {
    de = readdir(dir);

    if(de == NULL) break;
    else if(!strcmp(de->d_name, ".") || !strcmp("..", de->d_name)) continue;

    if(i >= len) {
      len *= 2;
      dirbuf = realloc(dirbuf, len * sizeof(char *));

      if(dirbuf == NULL)
	bomb("out of memory - can not reallocate directory buffer");
    }
     
    dirbuf[ i++ ] = strdup(de->d_name);
  }

  x = allocate(i);
  save(x);

  while(i--) {
    x = top();
    S_DATA(x)[ i ] = string(dirbuf[ i ]);
    free(dirbuf[ i ]);
  }

  closedir(dir);
  free(dirbuf);
  return restore();
}


XDEFINE(f_cd)
{
  chdir(check_string(x, "cd", 0));
  return x;
}


XDEFINE(f_mkdir)
{
  mkdir(check_string(x, "mkdir", 0));
  return x;
}


XDEFINE(f_rmdir)
{
  rmdir(check_string(x, "rmdir", 0));
  return x;
}


XDEFINE(f_rm)
{
  unlink(check_string(x, "rm", 0));
  return x;
}


XDEFINE(f_cwd)
{
  return string(getcwd(buffer, sizeof(buffer) - 1));
}


static X stat1(X arg, int link, char *loc)
{
  struct stat sb;
  char *fname = check_string(arg, loc, 0);
  int r = link ? lstat(fname, &sb) : stat(fname, &sb);
  X x, y;
  
  if(r != 0) return F;

  switch(sb.st_mode & S_IFMT) {
  case S_IFDIR: x = intern("DIR"); break;
#ifndef __MINGW32__
  case S_IFLNK: x = intern("LINK"); break;
  case S_IFIFO: x = intern("FIFO"); break;
  case S_IFSOCK: x = intern("SOCKET"); break;
#endif
  default: x = intern("NORMAL"); 
  }

  y = sequence(2, TO_N(sb.st_mtime >> 16), TO_N(sb.st_mtime & 0xffff));
  return sequence(3, x, TO_N(sb.st_size), y);
}


XDEFINE(f_stat)
{
  return stat1(x, 0, "stat");
}


XDEFINE(f_lstat)
{
  return stat1(x, 1, "lstat");
}


XDEFINE(f_time)
{
  time_t tm = time(NULL);
  return sequence(2, TO_N(tm >> 16), TO_N(tm & 0xffff));
}


XDEFINE(f_sleep)
{
  _sleep(check_N(x, "sleep"));
  return x;
}
