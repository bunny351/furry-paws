/* sdl.c - SDL interface for FP */


#ifdef __MINGW32__
#include <windows.h>
#endif

#include <SDL/SDL.h>
#ifdef SDL_TTF
# include <SDL/SDL_ttf.h>
#endif
#ifdef SDL_IMAGE
# include <SDL/SDL_image.h>
#endif
#ifdef SDL_GFX
# include <SDL_gfxPrimitives.h>
# include <SDL_rotozoom.h>
#endif
#ifdef SDL_PICOFONT
# include <SDL_picofont.h>
#endif


static SDL_Surface *screen;
static int screen_width, screen_height;

struct resource {
  X name;			/* atom */
  void *data;
  struct resource *next;
};

static struct resource *image_list, *font_list;
static X BOX_atom, IMAGE_atom, TEXT_atom, LINE_atom;
static X COLOR_atom, BGCOLOR_atom, FONT_atom;
static X KEY_atom, BUTTON_atom, MOUSE_atom, QUIT_atom;
static X NOFRAME_atom, RESIZABLE_atom, FRAME_atom, FIXED_atom;
static Uint32 current_color;
static Uint32 current_bgcolor;
#ifdef SDL_TTF
static TTF_Font *current_font;
static Uint16 *text_buffer;
#else
static char *text_buffer;
#endif
static int text_buffer_size;
static int initialized = 0;
static int screen_flags = 0;


/* graphics : <width, height, [NOFRAME], [RESIZABLE]> -> bool */

XDEFINE(graphics)
{
  X *args = check_Smin(x, 2, "graphics");
  int i;

  if(!initialized) {
    BOX_atom = intern("BOX");
    IMAGE_atom = intern("IMAGE");
    TEXT_atom = intern("TEXT");
    LINE_atom = intern("LINE");
    COLOR_atom = intern("COLOR");
    BGCOLOR_atom = intern("BGCOLOR");
    FONT_atom = intern("FONT");
    KEY_atom = intern("KEY");
    BUTTON_atom = intern("BUTTON");
    MOUSE_atom = intern("MOUSE");
    QUIT_atom = intern("QUIT");
    NOFRAME_atom = intern("NOFRAME");
    FRAME_atom = intern("FRAME");
    FIXED_atom = intern("FIXED");
    RESIZABLE_atom = intern("RESIZABLE");
    
    if(SDL_Init(SDL_INIT_VIDEO) < 0) 
      fail("can not initialize SDL");
    
    SDL_EnableUNICODE(1);

    if(SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL) < 0)
      fail("can not set SDL key repeat");

#ifdef SDL_TTF
    TTF_Init();
#endif
    image_list = font_list = NULL;
#ifdef SDL_TTF
    current_font = NULL;
#endif
    text_buffer = NULL;
  }

  screen_width = check_N(args[ 0 ], "graphics");
  screen_height = check_N(args[ 1 ], "graphics");

  for(i = 2; i < S_LENGTH(x); ++i) {
    if(args[ i ] == NOFRAME_atom)
      screen_flags |= SDL_NOFRAME;
    else if(args[ i ] == FRAME_atom)
      screen_flags |= SDL_NOFRAME;
    if(args[ i ] == RESIZABLE_atom)
      screen_flags |= SDL_RESIZABLE;
    if(args[ i ] == FIXED_atom)
      screen_flags &= ~SDL_RESIZABLE;
    if(args[ i ] == FRAME_atom)
      screen_flags &= ~SDL_NOFRAME;
  }

  screen = SDL_SetVideoMode(screen_width, screen_height, 0, screen_flags);

  if(!initialized) {
    current_color = SDL_MapRGB(screen->format, 0, 0, 0);
    current_bgcolor = SDL_MapRGB(screen->format, 255, 255, 255);
    initialized = 1;
  }

  if(screen == NULL)
    fail("can not set SDL video mode");

  return T;
}


static void *
find_resource(X name, struct resource *list)
{
  while(list != NULL) {
    if(list->name == name) return list->data;
    else list = list->next;
  }

  return NULL;
}


static struct resource *
register_resource(X name, void *data, struct resource *list)
{
  struct resource *r = (struct resource *)malloc(sizeof(struct resource *));

  if(r == NULL)
    fail("out of memory - can not allocate resource bucket");

  r->name = name;
  r->data = data;
  r->next = list;
  return r;
}


#ifdef SDL_TTF
static Uint16 *
unicode_text(X str)
{
  int len = S_LENGTH(str);
  int i;

  if(text_buffer == NULL || text_buffer_size <= len) {
    text_buffer = (Uint16 *)realloc(text_buffer, (len + 1) * sizeof(Uint16));
    text_buffer_size = len;
  }

  for(i = 0; i < len; ++i)
    text_buffer[ i ] = N_VALUE(S_DATA(str)[ i ]);

  text_buffer[ i ] = 0;
  return text_buffer;
}
#endif


#ifdef SDL_PICOFONT
static char *
ascii_text(X str)
{
  int len = S_LENGTH(str);
  int i;

  if(text_buffer == NULL || text_buffer_size <= len) {
    text_buffer = (char *)realloc(text_buffer, (len + 1) * sizeof(char));
    text_buffer_size = len;
  }

  for(i = 0; i < len; ++i)
    text_buffer[ i ] = N_VALUE(S_DATA(str)[ i ]);

  text_buffer[ i ] = 0;
  return text_buffer;
}
#endif


static void *
find_image(X name)
{
  void *data = find_resource(name, image_list);
  void *data2;

  if(data == NULL) {
    char *iname = check_string(name, "(graphics)", 0);
#ifdef SDL_IMAGE
    data = (void *)IMG_Load(iname);
#else
    data = (void *)SDL_LoadBMP(iname);
#endif

    if(data == NULL)
      failx(name, "can not load image");

    data2 = SDL_DisplayFormatAlpha((SDL_Surface *)data);
    SDL_FreeSurface(data);
    data = data2;
    image_list = register_resource(name, data, image_list);
  }

  return data;
}


static void
render_rec(SDL_Surface *screen, char *loc, X x)
{
  X *tree = check_S(x, loc);
  SDL_Rect r;

  if(S_LENGTH(x) == 0) return;

  if(tree[ 0 ] == BOX_atom) {	/* <BOX, x, y, w, h> */
    X *box = check_Sn(x, 5, loc);

    r.x = N_VALUE(box[ 1 ]);
    r.y = N_VALUE(box[ 2 ]);
    r.w = N_VALUE(box[ 3 ]);
    r.h = N_VALUE(box[ 4 ]);
    SDL_FillRect(screen, &r, current_color);
  }
  else if(tree[ 0 ] == LINE_atom) {	/* <LINE, x1, y1, x2, y2> */
    X *line = check_Sn(x, 5, loc);

#ifdef SDL_GFX
    lineColor(screen, N_VALUE(line[ 1 ]), N_VALUE(line[ 2 ]), 
	      N_VALUE(line[ 3 ]), N_VALUE(line[ 4 ]),
	      current_color);
#else
    fprintf(stderr, "* LINE graphic operation not supported\n");
#endif
  }
  else if(tree[ 0 ] == IMAGE_atom) { /* <IMAGE, name, x, y [, angle*1000] [, zoomx*1000, zoomy*1000]> */
    X *image = check_Smin(x, 4, loc);
    check_string(image[ 1 ], loc, 0);
    void *data = find_image(image[ 1 ]);
    double a = 0, zx = 1, zy = 1;
    int f = 0;

    r.x = check_N(image[ 2 ], loc);
    r.y = check_N(image[ 3 ], loc);

    switch(S_LENGTH(x)) {
    case 4: break;

    case 5: 
      a = (double)check_N(image[ 4 ], loc) / 1000; 
      break;

    case 6: 
      zx = (double)check_N(image[ 4 ], loc) / 1000;
      zy = (double)check_N(image[ 5 ], loc) / 1000;
      break;

    case 7:
      a = (double)check_N(image[ 4 ], loc) / 1000; 
      zx = (double)check_N(image[ 5 ], loc) / 1000;      
      zy = (double)check_N(image[ 6 ], loc) / 1000;
    }

#ifdef SDL_GFX
    if(a != 0 || zx != 1 || zy != 1) {
      data = rotozoomSurface((SDL_Surface *)data, a, zx, zy);
      f = 1;
    }
#endif

    SDL_BlitSurface((SDL_Surface *)data, NULL, screen, &r);

    if(f) SDL_FreeSurface((SDL_Surface *)data);
  }
  else if(tree[ 0 ] == TEXT_atom) { /* <TEXT, string, x, y> */
    X *text = check_Sn(x, 4, loc);
    SDL_Surface *s;
    SDL_Color c;

    SDL_GetRGB(current_color, screen->format, &c.r, &c.g, &c.b);
    check_string(text[ 1 ], loc, 1);
#ifdef SDL_TTF
    s = TTF_RenderUNICODE_Blended(current_font, unicode_text(text[ 1 ]), c);
#elif defined(SDL_PICOFONT)
    s = FNT_Render(ascii_text(text[ 1 ]), c);
#else
    fprintf(stderr, "* TEXT graphic operation not supported\n");
#endif
    r.x = check_N(text[ 2 ], loc);
    r.y = check_N(text[ 3 ], loc);
    SDL_BlitSurface(s, NULL, screen, &r);
    SDL_FreeSurface(s);
  }
  else if(tree[ 0 ] == FONT_atom) { /* <FONT, name, size> */
#ifdef SDL_TTF
    X *font = check_Sn(x, 3, loc);
    char *name = check_A(font[ 1 ], loc);
    void *data = find_resource(font[ 1 ], font_list);
    SDL_Surface *s;
    SDL_Color c;

    if(data == NULL) {
      data = (void *)TTF_OpenFont(name, check_N(font[ 2 ], loc));

      if(data == NULL)
	failx(x, "can not load font `%s'", name);

      font_list = register_resource(font[ 1 ], data, font_list);
    }

    current_font = data;
#endif
  }
  else if(tree[ 0 ] == COLOR_atom) { /* <COLOR, r, g, b> */
    X *color = check_Sn(x, 4, loc);
    current_color = SDL_MapRGB(screen->format, 
			       check_N(color[ 1 ], loc),
			       check_N(color[ 2 ], loc),
			       check_N(color[ 3 ], loc));
  }
  else if(tree[ 0 ] == BGCOLOR_atom) { /* <BGCOLOR, r, g, b> */
    X *color = check_Sn(x, 4, loc);
    current_bgcolor = SDL_MapRGB(screen->format, 
			         check_N(color[ 1 ], loc),
			         check_N(color[ 2 ], loc),
			         check_N(color[ 3 ], loc));
  }
  else {
    int i, len = S_LENGTH(x);
    
    for(i = 0; i < len; ++i)
      render_rec(screen, loc, S_DATA(x)[ i ]);
  }
}


/* render1 : [tree, clear?, flip?] -> tree */

XDEFINE(render1)
{
  X *args = check_Sn(x, 3, "render1");
  SDL_Rect r = { 0, 0, screen_width, screen_height };

  if(args[ 1 ] != F)
    SDL_FillRect(screen, &r, current_bgcolor);

  render_rec(screen, "render1", args[ 0 ]);

  if(args[ 2 ] != F) SDL_Flip(screen);
}


/* textsize : <string, font, size> -> <width, height> 

   font + size will be ignored with picofont.
*/

XDEFINE(textsize)
{
  X *args = check_Sn(x, 3, "textsize");
  int w, h;

#ifdef SDL_TTF
  TTF_Font *font;
  Uint16 *text = unicode_text(check_string(args[ 0 ], "textsize", 0));
  char *name = check_A(args[ 1 ], "textsize");
  void *data = find_resource(args[ 1 ], font_list);

  if(data == NULL) {
    data = (void *)TTF_OpenFont(name, check_N(args[ 2 ], "textsize"));

    if(data == NULL)
      failx(x, "can not load font `%s'", name);

    font_list = register_resource(args[ 1 ], data, font_list);
  }
  
  TTF_SizeUNICODE(font, text, &w, &h);
  return sequence(2, TO_N(w), TO_N(h));
#elif defined(SDL_PICOFONT)
  char *text = ascii_text(check_string(args[ 0 ], "textsize", 1));
  int r, c;

  for(w = r = c = 0; *text != '\0'; ++text) {
    switch(*text) {
    case '\t': c += 8 - (c % 8); break;
    case '\n': w = c > w ? c : w; ++r; break;
    }
  }

  return sequence(2, TO_N(w * 8), TO_N(r * 8));
#else
  return F;
#endif
}


/* imagesize : image_name -> <w, h> */

XDEFINE(imagesize) 
{
  check_string(x, "image_size", 0);
  void *data = find_image(x);
  
  return sequence(2, TO_N(((SDL_Surface *)data)->w), TO_N(((SDL_Surface *)data)->h));
}


/* event : delay -> event | F */

XDEFINE(event)
{
  SDL_Event event;
  int d = IS_N(x) ? N_VALUE(x) : 0;
  int f = 1;

  for(;;) {
    if(x == T) SDL_WaitEvent(&event);
    else f = SDL_PollEvent(&event);

    if(f) {
      switch(event.type) {
      case SDL_QUIT:
	return sequence(1, QUIT_atom);

      case SDL_KEYDOWN:
      case SDL_KEYUP:
	return sequence(4, KEY_atom, TO_B(event.key.state == SDL_PRESSED), 
			TO_N(event.key.keysym.sym),
			TO_N(event.key.keysym.unicode));

      case SDL_MOUSEBUTTONUP:
      case SDL_MOUSEBUTTONDOWN:
	return sequence(4, BUTTON_atom, TO_B(event.button.state == SDL_PRESSED),
			TO_N(event.button.x), TO_N(event.button.y));

      case SDL_MOUSEMOTION:
	return sequence(3, MOUSE_atom, TO_N(event.motion.x), TO_N(event.motion.y));

      case SDL_VIDEORESIZE:
	screen_width = event.resize.w;
	screen_height = event.resize.h;
	screen = SDL_SetVideoMode(screen_width, screen_height, 0, screen_flags);
	break;
      }
    }

    if(d != 0) usleep(d * 1000);
    else break;
  }

  return F;
}


/* keyrepeat : <delay, interval> */

XDEFINE(keyrepeat)
{
  X *a = check_Sn(x, 2, "keyrepeat");
  int d = check_N(a[ 0 ], "keyrepeat");
  int i = check_N(a[ 1 ], "keyrepeat");

  if(SDL_EnableKeyRepeat(d, i) < 0)
    failx(x, "can not set SDL key repeat");

  return F;
}


#ifdef __MINGW32__
static int
parse_argv(char *cmds, char **argv)
{
  char *ptr = cmds, *bptr0, *bptr, *aptr;
  int n = 0;
  int argc = 1;
  static char buffer[ 1024 ];
  
  argv[ 0 ] = "<me>";

  for(;;) {
    while(isspace(*ptr)) ++ptr;

    if(*ptr == '\0') return argc;

    for(bptr0 = bptr = buffer; !isspace(*ptr) && *ptr != '\0'; *(bptr++) = *(ptr++))
      ++n;
    
    *bptr = '\0';
    aptr = (char *)malloc(n + 1);
    
    if(aptr == NULL)
      bomb("cannot allocate argument buffer");

    strcpy(aptr, bptr0);
    argv[ argc++ ] = aptr;
  }
}


int WINAPI 
WinMain(HINSTANCE me, HINSTANCE you, LPSTR cmdline, int show) 
{
  static char *argv[ 1024 ];
  int argc = parse_argv(cmdline, argv);

  return main(argc, argv);
}
#endif
