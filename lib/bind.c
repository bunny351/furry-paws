/* bind.c - dynamic-linking */


#ifdef __MINGW32__
# include <windows.h>
# define MODULE_HANDLE HMODULE
#else
# include <stdlib.h>
# include <dlfcn.h>
# define MODULE_HANDLE void *
#endif

#include <string.h>


struct dlib {
  char *name;
  MODULE_HANDLE *h;
  struct dlib *next;
};

static struct dlib *modules = NULL;


void *bind(char *mname, char *sname, char **error)
{
  struct dlib *dlp;
  char *err;
  void *ptr;

  for(dlp = modules; dlp != NULL; dlp = dlp->next)
    if(!strcmp(dlp->name, mname)) break;

  if(dlp == NULL) {
    HMODULE_HANDLE mod;
#ifdef __MINGW32__
    mod = LoadLibrary(mname);
#else
    mod = dlopen(mname, RTLD_GLOBAL | RTLD_LAZY);
#endif

    if(mod == NULL) goto fail;

    struct dlib *dlp2 = (struct dlib *)malloc(sizeof(struct dlib));
    assert(dlp2);
    dlp2->name = strdup(mname);
    dlp2->h = mod;
    dlp2->next = modules;
    modules = dlp2;
  }

#ifdef __MINGW32__
  ptr = GetProcAddress(mod, sname);
#else
  ptr = dlsym(mod, sname);
#endif

  if(ptr == NULL) {
  fail:
    if(error != NULL) {
#ifdef __MINGW32__
      LPTSTR buf;
      DWORD c = GetLastError();
      DWORD n = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | 
			      FORMAT_MESSAGE_ALLOCATE_BUFFER, NULL, c, 0, 
			      (LPTSTR)error, 0, NULL);
#else
      error = dlerror();
#endif
    }

    return NULL;
  }

  return ptr;
}
