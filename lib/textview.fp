%
% textview.fp
%

% state = <position, offset, lines>
% position = offset = <row, column>
% lines = <line1, ...>
% line = <str, prop1, ...>
% prop = <propname, propval>

tv = {

  % textview : text -> state
  textview = [~<1, 1>, ~<0, 0>, @make_line.split.[~10, id]]

  make_line = [id]
  line_string = s1

  current_position = s1
  current_offset = s2
  current_lines = s3

  % display : state
  %
  % XXX currently ignores offset and font
  % XXX limit rendered lines to height of display
  display = g:render.y.\display1.al.[~<0, <>>, current_lines]
  display1 = gt.[s1.x, h] -> s2.x;
  	     [add.[s1.x, line_height.y],
	      ar.[s2.x, [~TEXT, line_string.y, ~0, s1.x]] ]

  line_height = y.g:textsize.[line_string, #, #]

}
