%
% graphics.fp - simple SDL interface
%

% XXX use monadic I/O


"lib/sdl.c"


g = {

  graphics = extern `graphics`
  render1 = extern `render1`
  render = render1.[id, ~T, ~T]
  render_over = render1.[id, ~F, ~T]
  render_incremental = render1.[id, ~F, ~F]
  event = extern `event`
  imagesize = extern `imagesize`
  textsize = extern `textsize`
  keyrepeat = extern `keyrepeat`

  % center : graphics(image/text) -> graphics'
  center = [|?IMAGE, ..|] -> center_image;
	   [|?TEXT, ..|] -> center_text

  center_image = cat.[[~IMAGE, s2],
		      center_position.[id, imagesize.s2],
		      drop.[~4, id]]
  center_text = cat.[[~TEXT, s2],
		     center_position.[id, textsize.[s2, #, #]], % works only for picofont
		     drop.[~4, id]]
  center_position = [sub.[s3.x, bshr.[x.y, ~1]],
		     sub.[s4.x, bshr.[y.y, ~1]] ]

}
